HOST = "core-mosquitto" # changer si on n'utilise pas l'addon
USER = "mqtt_user_name" # peut être un utilisateur HA avec l'addon
PASS = "pass_word"

carburants = [
        "Gazole",
        "E10",
    ]
positions = [
        # lat, lon, rayon
        (46.0045, 4.71966, 7000),
        (45.917, 4.775, 500),
    ]


import paho.mqtt.publish as publish
from itertools import product
from urllib.request import urlopen
import json
import time
import datetime

sensors = []
values = []

now = datetime.datetime.utcnow().isoformat() + "+00:00"


for prix_nom, (lat, lon, dist) in product(carburants, positions):
    data = json.loads(urlopen(
        "https://data.economie.gouv.fr/api/records/1.0/search/?"
        "dataset=prix-carburants-fichier-instantane-test-ods-copie&"
        "q=&rows=1000&sort=-prix_valeur&facet=prix_nom&"
        f"refine.prix_nom={prix_nom}&"
        f"geofilter.distance={lat}%2C{lon}%2C{dist}"
        ).read())

    for rec in data["records"]:
        r = rec["fields"]
        name = f"{r['id']:>08}_{r['prix_nom']}"
        sensors.append({
            "topic": f"homeassistant/sensor/carburant_{name}/config",
            "payload": json.dumps({
                "device": {
                    "name": "Carburants",
                    "identifiers": "carburants"
                    },
                "state_topic": f"Carburants/{name}",
                "unit_of_measurement": "€/L",
                "device_class": "monetary",
                "icon": "mdi:gas-station",
                "value_template": "{{ value_json['price'] }}",
                "expire_after": 3600,
                "json_attributes_topic": f"Carburants/{name}",
                "unique_id": f"carburant_{name}",
                "object_id": f"carburant_{name}",
                "name": f"{r['prix_nom']} à {r['adresse']}",
                })
            })
        values.append({
            "topic": f"Carburants/{name}",
            "payload": json.dumps({
                "price": r["prix_valeur"],
                "fuel": r["prix_nom"],
                "localization": f"{r['adresse']} {r['com_arm_name']}",
                "updated": r["prix_maj"],
                "fetched": now,
                "id": r["id"],
                "latitude": r["geom"][0],
                "longitude": r["geom"][1],
                "distance": r["dist"],
                })
            })

for msgs in (sensors, values):
    if not msgs:
        pass
    publish.multiple(msgs, hostname=HOST,
        auth={"username": USER, "password": PASS})
    time.sleep(10)
